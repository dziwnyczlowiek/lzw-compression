import sys
import os
import docx
from docx.shared import RGBColor

instrukcja = "lzw.py [plik z tekstem/tekst] [k/d]\nk - kompresja\nd - dekompresja"


# Funkcja główna
def main():
    # Jeśli liczba argumentów jest nieodpowiednia, przestań realizować program
    if len(sys.argv) < 3:
        print("Brak odpowiedniej liczby argumentów")
        print(instrukcja)
        return

    # Jeśli drugi argument nie jest k lub d, przerwij program
    if sys.argv[2] != "k" and sys.argv[2] != "d":
        print("Nieprawidłowy tryb kompresja/dekompresja")
        print(instrukcja)
        return

    # Tryb kompresji
    if sys.argv[2] == "k":
        kompresja()

    # Tryb dekompresji
    if sys.argv[2] == "d":
        dekompresja()


def kompresja():
    # Tekst do kompresji
    tekst = ""

    # Jeśli podano plik to otwórz go i wstaw do zmiennej tekst
    if os.path.isfile(sys.argv[1]):
        plik = open(sys.argv[1], "r", encoding="utf-8")
        tekst = plik.read()
        plik.close()
    # Jeśli argument nie jest ścieżką pliku to potraktuj go jako surowy tekst
    else:
        tekst = sys.argv[1]

    # Zainicjuj dokument word
    dokumentWord = docx.Document()

    # Zainicjuj słownik z pierwszym ciągiem NULL
    slownik = [None]

    # Stwórz tabelę word dla słownika wstępnego
    tabelaSlownikWstepny = dokumentWord.add_table(rows=1, cols=2)
    tabelaSlownikWstepny.rows[0].cells[0].text = "Index"
    tabelaSlownikWstepny.rows[0].cells[1].text = "Słowo"

    # Wypełnij słownik pojedynczymi ciągami
    for litera in tekst:
        if litera not in slownik:
            wiersz = tabelaSlownikWstepny.add_row().cells
            wiersz[0].text = str(len(slownik))
            wiersz[1].text = litera
            slownik.append(litera)

    dokumentWord.add_paragraph()

    # Stwórz tabelę słownika wygenerowanego
    tabelaSlownikWygenerowany = dokumentWord.add_table(rows=1, cols=2)
    tabelaSlownikWygenerowany.rows[0].cells[0].text = "Index"
    tabelaSlownikWygenerowany.rows[0].cells[1].text = "Słowo"

    dokumentWord.add_paragraph()

    # Pierwszy symbol wejściowy
    poprzedniCiag = tekst[0]

    # Tekst skompresowany
    tekstSkompresowany = ""

    # Stwórz paragraf
    paragraf = dokumentWord.add_paragraph()

    # Działaj aż skończy się tekst
    i = 1
    while i < len(tekst):

        # Pobierz bieżący symbol
        biezacySymbol = tekst[i]

        # Jeśli poprzedniCiag + biezacySymbol nie są w słowniku
        if not poprzedniCiag + biezacySymbol in slownik:

            # Dodaj wiersz do tabeli słownika wygenerowanego
            wiersz = tabelaSlownikWygenerowany.add_row().cells
            wiersz[0].text = str(len(slownik))
            wiersz[1].text = poprzedniCiag + biezacySymbol

            # Zwróć indeks poprzedniego ciągu do tekstu skompresowanego
            # i dodaj do słownika kombinację poprzedniCiag + biezacySymbol
            index = slownik.index(poprzedniCiag)
            tekstSkompresowany += f"{index}, "
            slownik.append(poprzedniCiag + biezacySymbol)

            # Dodaj tekst do paragrafu
            paragraf.add_run(poprzedniCiag).bold = True
            paragraf.add_run(" w słowniku, ")
            paragraf.add_run(poprzedniCiag + biezacySymbol).font.color.rgb = RGBColor(0xff, 0x00, 0x00)
            paragraf.add_run(" - nie, ")
            paragraf.add_run(poprzedniCiag + biezacySymbol).font.color.rgb = RGBColor(0xff, 0x00, 0x00)
            paragraf.add_run(" do słownika, kod wyjścia ")
            paragraf.add_run(f"({str(index)})").font.color.rgb = RGBColor(0x00, 0x99, 0xff)

            # Nowy paragraf
            paragraf = dokumentWord.add_paragraph()

            # Zamień poprzedni ciąg na bieżacy symbol
            poprzedniCiag = biezacySymbol
        # W przeciwnym wypadku
        else:
            # poprzedni ciąg to poprzedniCiag + biezacySymbol
            poprzedniCiag = poprzedniCiag + biezacySymbol

        i += 1

    index = slownik.index(poprzedniCiag)
    tekstSkompresowany += f"{index}, "

    # Dodaj tekst do paragrafu
    paragraf.add_run(poprzedniCiag).bold = True
    paragraf.add_run(" w słowniku, ")
    paragraf.add_run("EOF, ").font.color.rgb = RGBColor(0x33, 0xcc, 0x33)
    paragraf.add_run("kod wyjścia ")
    paragraf.add_run(f"({str(index)})").font.color.rgb = RGBColor(0x00, 0x99, 0xff)

    dokumentWord.add_paragraph()

    dokumentWord.add_paragraph(tekstSkompresowany)

    dokumentWord.save("LZW.docx")

def dekompresja():

    # Kod kompresji
    calyKod = ""
    alfabetZrodla = ""

    # Jeśli argument jest plikiem to otwórz i przeczytaj go
    if (os.path.isfile(sys.argv[1])):
        with open(sys.argv[1], "r", encoding="utf-8") as file:
            alfabetZrodla = file.readline()
            alfabetZrodla = alfabetZrodla.strip("]")
            calyKod = file.read()
            calyKod = calyKod.rstrip(", ")
    # W preciwnym wypadku przerwij działanie programu
    else:
        print("Argument 1 nie jest plikiem")
        return

    # Zainicjuj słownik z pierwszym ciągiem NULL
    slownik = [None]

    # Zainicjuj dokument word
    dokumentWord = docx.Document()

    # Stwórz tabelę word dla słownika wstępnego
    tabelaSlownikWstepny = dokumentWord.add_table(rows=1, cols=2)
    tabelaSlownikWstepny.rows[0].cells[0].text = "Index"
    tabelaSlownikWstepny.rows[0].cells[1].text = "Słowo"

    # Wypełnij słownik alfabetem źródła
    for litera in alfabetZrodla:
        wiersz = tabelaSlownikWstepny.add_row().cells
        wiersz[0].text = str(len(slownik))
        wiersz[1].text = litera
        slownik.append(litera)

    dokumentWord.add_paragraph()

    # Stwórz tabelę słownika wygenerowanego
    tabelaSlownikWygenerowany = dokumentWord.add_table(rows=1, cols=2)
    tabelaSlownikWygenerowany.rows[0].cells[0].text = "Index"
    tabelaSlownikWygenerowany.rows[0].cells[1].text = "Słowo"

    dokumentWord.add_paragraph()

    # Uzyskaj listę kodów
    calyKod = calyKod.split(", ")

    # Wypisz ciąg związany z pierwszym kodem
    zdekompresowanyTekst = slownik[int(calyKod[0])]

    # Ciąg skojarzony z poprzednim kodem
    poprzedniKod = int(calyKod[0])

    # Paragraf do zapisu dekodowania
    paragraf = dokumentWord.add_paragraph()

    i = 1
    while i < len(calyKod):

        # Ciąg skojarzony z bieżacym kodem
        biezacyKod = int(calyKod[i])

        if int(biezacyKod) < len(slownik):
            slownik.append(slownik[poprzedniKod] + slownik[biezacyKod][0])
            zdekompresowanyTekst += slownik[biezacyKod]
            paragraf.add_run(str(poprzedniKod)).bold = True
            paragraf.add_run(" => ")
            paragraf.add_run(slownik[poprzedniKod]).font.color.rgb = RGBColor(0xff, 0x00, 0x00)
            paragraf.add_run(f", {poprzedniKod} {biezacyKod} = ")
            paragraf.add_run(slownik[poprzedniKod] + slownik[biezacyKod][0]).font.color.rgb = RGBColor(0x00, 0x99, 0xff)
            paragraf.add_run(" => słownik ")
            paragraf.add_run("nie").font.color.rgb = RGBColor(0xff, 0x00, 0x00)
            paragraf.add_run(", do słownika ")
            paragraf.add_run(f"({slownik[poprzedniKod] + slownik[biezacyKod][0]})").font.color.rgb = RGBColor(0x00, 0x99, 0xff)
            paragraf.add_run(", output ")
            paragraf.add_run(f"({slownik[poprzedniKod]})").font.color.rgb = RGBColor(0x33, 0xcc, 0x33)
            paragraf.add_run(";")
            paragraf = dokumentWord.add_paragraph()
        else:
            slownik.append(slownik[poprzedniKod] + slownik[poprzedniKod][0])
            zdekompresowanyTekst += slownik[poprzedniKod] + slownik[poprzedniKod][0]
            paragraf.add_run(str(poprzedniKod)).bold = True
            paragraf.add_run(" => ")
            paragraf.add_run(slownik[poprzedniKod]).font.color.rgb = RGBColor(0xff, 0x00, 0x00)
            paragraf.add_run(f", {poprzedniKod} {biezacyKod} = ")
            paragraf.add_run(slownik[poprzedniKod] + slownik[poprzedniKod][0]).font.color.rgb = RGBColor(0x00, 0x99, 0xff)
            paragraf.add_run(" => słownik ")
            paragraf.add_run("nie").font.color.rgb = RGBColor(0xff, 0x00, 0x00)
            paragraf.add_run(", do słownika ")
            paragraf.add_run(f"({slownik[poprzedniKod] + slownik[poprzedniKod][0]})").font.color.rgb = RGBColor(0x00,
                                                                                                              0x99,
                                                                                                              0xff)
            paragraf.add_run(", output ")
            paragraf.add_run(f"({slownik[poprzedniKod]})").font.color.rgb = RGBColor(0x33, 0xcc, 0x33)
            paragraf.add_run(";")
            paragraf = dokumentWord.add_paragraph()

        poprzedniKod = biezacyKod

        i += 1

    paragraf.add_run(str(poprzedniKod)).bold = True
    paragraf.add_run(" => ")
    paragraf.add_run(slownik[poprzedniKod]).font.color.rgb = RGBColor(0xff, 0x00, 0x00)
    paragraf.add_run(", ")
    paragraf.add_run("EOF").font.color.rgb = RGBColor(0x33, 0xcc, 0x33)
    paragraf.add_run(", output ")
    paragraf.add_run(f"({slownik[poprzedniKod]})").font.color.rgb = RGBColor(0x33, 0xcc, 0x33)
    paragraf.add_run(";")

    dokumentWord.add_paragraph()

    paragraf = dokumentWord.add_paragraph(zdekompresowanyTekst)

    dokumentWord.save("Tekst Zdekodowany.docx")



# Press the green button in the gutter to run the script.
if __name__ == "__main__":
    main()
